#!/usr/bin/env python

import re
import sys
import os
import acoustid
import chromaprint
from optparse import OptionParser
from PyQt4 import QtGui


def s32tou(x):
    return x % 2**32


parser = OptionParser()
parser.add_option("-a", "--audio", dest="audio_file",
                  help="calculate fingerprint for an audio file", metavar="FILE")
parser.add_option("-t", "--text", dest="text_file",
                  help="use fingerprint from a text file", metavar="FILE")
parser.add_option("-o", "--output", dest="output_file", default="fp.png",
                  help="write the image into this file (default: fp.png)", metavar="FILE")

(options, args) = parser.parse_args()


if options.audio_file:
    fp = acoustid.fingerprint_file(options.audio_file)[1]
    fp, algorithm = chromaprint.decode_fingerprint(fp)
elif options.text_file:
    file = sys.stdin if options.text_file == '-' else open(options.text_file)
    contents = file.read().strip()
    fp = re.search(r'FINGERPRINT=(\S+)', contents).group(1)
    fp, algorithm = chromaprint.decode_fingerprint(fp)
else:
    parser.error('no input file')


image = QtGui.QImage(32, len(fp), QtGui.QImage.Format_Mono)

for y, bits in enumerate([s32tou(x) for x in fp]):
    for x in range(32):
        index = 0
        if bits & (1 << x):
            index = 1
        image.setPixel(x, y, index)

image.save(options.output_file)

